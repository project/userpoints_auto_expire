USERPOINTS AUTO EXPIRE
=========================
This module adds functionality to reduce userpoint each time a user performs an extension of node expiry period

Install
=========================
1) Simply drop this module's directory into your modules directory 
2) Install dependences (userpoints and  Auto Expire)
3) Install via admin/build/modules.

Settings
=========================
Configure the Points for auto expire nodes options as per your requirements.

Author
=========================
Juan Vizcarrondo (http://drupal.org/user/352652) 
